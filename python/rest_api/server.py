from flask import (
    Flask,
    render_template
)

app = Flask(__name__, template_folder='templates')


@app.route("/")
def home():
    """
    Return the home page

    :return: rendered of the home.html
    """
    return render_template('home.html')


if __name__ == '__main__':
    app.run(debug=True)
